
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albumproject.service;

import com.albumproject.dao.ArtistDao;
import com.albumproject.dao.SaleDao;
import com.albumproject.model.ReportArtist;
import com.albumproject.model.ReportSale;
import java.util.List;


/**
 *
 * @author acer
 */
public class ReportService {
    
     public List<ReportSale> getReportSaleByDay() {
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }

    public List<ReportSale> getReportSaleByMonth(int year) {
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }

    public List<ReportArtist> getReportArtistByMonth(int year) {
        ArtistDao dao = new ArtistDao();
        return dao.getReportArtistByMonth(year);
    }

}
