/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albumproject.poc;

import com.albumproject.model.ReportSale;
import com.albumproject.service.ReportService;
import java.util.List;

/**
 *
 * @author acer
 */
public class TestReportSale {
    public static void main(String[] args) {
        ReportService reportService = new ReportService();
//        List<ReportSale> report = reportService.getReportSaleByDay();
//        for (ReportSale r : report) {
//            System.out.println(r);
//        }
        List<ReportSale> report = reportService.getReportSaleByMonth(2013);
        for (ReportSale r : report) {
            System.out.println(r);
        }
    }
}
