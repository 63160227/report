/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albumproject.poc;

import com.albumproject.model.ReportArtist;
import com.albumproject.model.ReportSale;
import java.util.List;
import com.albumproject.service.ReportService;

/**
 *
 * @author acer
 */
public class TestReportArtist {
    
      public static void main(String[] args) {
        ReportService reportService = new ReportService();
        List<ReportArtist> report = reportService.getReportArtistByMonth(2013);
        for (ReportArtist r : report) {
            System.out.println(r);
        }
    }

}
