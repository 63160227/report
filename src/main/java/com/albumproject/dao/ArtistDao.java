/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.albumproject.dao;

import com.albumproject.helper.DatabaseHelper;
import com.albumproject.model.ReportArtist;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author acer
 */
public class ArtistDao {
    
public List<ReportArtist> getReportArtistByMonth(int year) {
        ArrayList<ReportArtist> list = new ArrayList();
        String sql = "SELECT artists.ArtistId,artists.Name as name ,(invoice_items.Quantity*invoice_items.UnitPrice) as total,strftime(\"%Y-%m\",InvoiceDate) as period\n"
                + "FROM artists INNER JOIN albums ON artists.ArtistId = albums.ArtistId\n"
                + "INNER JOIN tracks on albums.AlbumId = tracks.AlbumId\n"
                + "INNER JOIN invoice_items on tracks.TrackId = invoice_items.TrackId\n"
                + "INNER JOIN invoices on invoice_items.InvoiceId = invoices.InvoiceId\n"
                + "WHERE strftime(\"%Y\",InvoiceDate)=\"" + year + "\"\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportArtist item = ReportArtist.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}

